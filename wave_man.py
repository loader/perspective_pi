from tkinter import *

import time
import threading
from PIL import ImageTk, Image, ImageSequence

from base_canvas import BaseCanvas


class WaveMan(BaseCanvas):

    def __init__(self):
        super(WaveMan, self).__init__("wave_man")

        self.image_name = "./assets/wave_man/wave_man.gif"

        self.origin_image_width = 600
        self.origin_image_height = 944
        self.image_height = self.height
        self.image_width = int(self.height / self.origin_image_height * self.origin_image_width)

        self.image_view = self.canvas.create_image((self.width / 2, self.image_height / 2), anchor=CENTER)

    def __build_frames(self):
        self.frames = []
        im = Image.open(self.image_name)

        for frame in ImageSequence.Iterator(im):
            self.frames.append(ImageTk.PhotoImage(frame.convert('RGB').resize((self.image_width, self.image_height))))

    def __run(self):
        self.__build_frames()

        while True:
            for frame in self.frames:
                self.canvas.itemconfig(self.image_view, image=frame)
                self.root.update()
                time.sleep(0.05)

    def start(self):
        threading.Thread(target=self.__run).start()


if __name__ == '__main__':
    wave = WaveMan()
    wave.start()
    wave.root.mainloop()
