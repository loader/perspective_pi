# -*- coding:utf-8 -*-
import spidev as SPI
import ST7789
import RPi.GPIO as GPIO
import threading

# GPIO define
RST_PIN = 25
CS_PIN = 8
DC_PIN = 24

KEY_UP_PIN = 6
KEY_DOWN_PIN = 19
KEY_LEFT_PIN = 5
KEY_RIGHT_PIN = 26
KEY_PRESS_PIN = 13

KEY1_PIN = 21
KEY2_PIN = 20
KEY3_PIN = 16

RST = 27
DC = 25
BL = 24
bus = 0
device = 0
pressed_key = None

GPIO.setmode(GPIO.BCM)
GPIO.setup(KEY_UP_PIN,      GPIO.IN, pull_up_down=GPIO.PUD_UP) # Input with pull-up
GPIO.setup(KEY_DOWN_PIN,    GPIO.IN, pull_up_down=GPIO.PUD_UP) # Input with pull-up
GPIO.setup(KEY_LEFT_PIN,    GPIO.IN, pull_up_down=GPIO.PUD_UP) # Input with pull-up
GPIO.setup(KEY_RIGHT_PIN,   GPIO.IN, pull_up_down=GPIO.PUD_UP) # Input with pull-up
GPIO.setup(KEY_PRESS_PIN,   GPIO.IN, pull_up_down=GPIO.PUD_UP) # Input with pull-up
GPIO.setup(KEY1_PIN,        GPIO.IN, pull_up_down=GPIO.PUD_UP) # Input with pull-up
GPIO.setup(KEY2_PIN,        GPIO.IN, pull_up_down=GPIO.PUD_UP) # Input with pull-up
GPIO.setup(KEY3_PIN,        GPIO.IN, pull_up_down=GPIO.PUD_UP) # Input with pull-up


def listen(callback):

    def check_and_callback(key):
        global pressed_key

        if pressed_key is key:
            callback(KEY_UP_PIN)

        pressed_key = None

    def key_listen():
        global pressed_key

        while 1:
            # with canvas(device) as draw:
            if GPIO.input(KEY_UP_PIN):  # button is released
                check_and_callback(KEY_UP_PIN)
            else:  # button is pressed:
                pressed_key = KEY_UP_PIN

            if GPIO.input(KEY_LEFT_PIN):  # button is released
                check_and_callback(KEY_LEFT_PIN)
            else:  # button is pressed:
                pressed_key = KEY_LEFT_PIN

            if GPIO.input(KEY_RIGHT_PIN):  # button is released
                check_and_callback(KEY_RIGHT_PIN)
            else:  # button is pressed:
                pressed_key = KEY_RIGHT_PIN

            if GPIO.input(KEY_DOWN_PIN):  # button is released
                check_and_callback(KEY_DOWN_PIN)
            else:  # button is pressed:
                pressed_key = KEY_DOWN_PIN

            if GPIO.input(KEY_PRESS_PIN):  # button is released
                check_and_callback(KEY_PRESS_PIN)
            else:  # button is pressed:
                pressed_key = KEY_PRESS_PIN

            if GPIO.input(KEY1_PIN):  # button is released
                check_and_callback(KEY1_PIN)
            else:  # button is pressed:
                pressed_key = KEY1_PIN

            if GPIO.input(KEY2_PIN):  # button is released
                check_and_callback(KEY2_PIN)
            else:  # button is pressed:
                pressed_key = KEY2_PIN

            if GPIO.input(KEY3_PIN):  # button is released
                check_and_callback(KEY3_PIN)
            else:  # button is pressed:
                pressed_key = KEY3_PIN

    threading.Thread(target=key_listen).start()
