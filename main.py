from key_listener import listen


def key_callback(key):
    print("pressed: {}".format(key))


listen(key_callback)

while True:
    pass
