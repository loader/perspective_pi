from tkinter import *

import time
import threading

from base_canvas import BaseCanvas


class Clock(BaseCanvas):

    def __init__(self):
        super(Clock, self).__init__("clock")

        self.big_font = ('微软雅黑', 60, 'bold')
        self.small_font = ('微软雅黑', 20)

        self.weeks = ['周日', '周一', '周二', '周三', '周四', '周五', '周六']

        self.time_last_x_offset = 0
        self.time_last_y_offset = 0
        self.date_last_x_offset = 0
        self.date_last_y_offset = 0

        self.time_item = None
        self.date_item = None

    def __create_item(self):
        self.time_item = self.canvas.create_text(0, 0, text='',
                                            font=self.big_font,
                                            fill='white',
                                            anchor=W,
                                            justify=LEFT)

        self.date_item = self.canvas.create_text(0, 0, text='',
                                            font=self.small_font,
                                            fill='white',
                                            anchor=W,
                                            justify=LEFT)

    def __center(self):
        self.canvas.move(self.time_item, -self.time_last_x_offset, -self.time_last_y_offset)
        self.canvas.move(self.date_item, -self.date_last_x_offset, -self.date_last_y_offset)

        coords = self.canvas.bbox(self.time_item)
        self.time_last_x_offset = (self.width / 2) - ((coords[2] - coords[0]) / 2)
        self.time_last_y_offset = 120

        self.canvas.move(self.time_item, self.time_last_x_offset, self.time_last_y_offset)

        coords = self.canvas.bbox(self.date_item)
        self.date_last_x_offset = (self.width / 2) - ((coords[2] - coords[0]) / 2)
        self.date_last_y_offset = 180

        self.canvas.move(self.date_item, self.date_last_x_offset, self.date_last_y_offset)

    def __timer(self):
        self.__create_item()

        while True:
            cur_time = time.localtime()

            month = time.strftime("%m月", cur_time)
            if month.startswith("0"):
                month = month[1:]

            day = time.strftime("%d日", cur_time)
            if day.startswith("0"):
                day = day[1:]

            week = self.weeks[int(time.strftime("%w", cur_time))]
            t = time.strftime("%H:%M", cur_time)

            self.canvas.itemconfig(self.time_item, text=t)
            self.canvas.itemconfig(self.date_item, text=month + day + week)

            self.__center()

            self.root.update()
            time.sleep(0.1)

    def start(self):
        threading.Thread(target=self.__timer).start()


if __name__ == '__main__':
    clock = Clock()
    clock.start()
    clock.root.mainloop()
