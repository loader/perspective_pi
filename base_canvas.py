from tkinter import *


class BaseCanvas(object):

    def __init__(self, title):
        self.root = Tk()
        self.root.title(title)
        self.root.overrideredirect(1)
        # self.root.overrideredirect(0)

        # self.root.wm_attributes('-type', 'splash')
        self.width = 320  # self.root.winfo_width()
        self.height = 320  # self.root.winfo_height()

        self.canvas = Canvas(self.root, width=self.width, height=self.height, background='black')
        self.canvas.pack()
